from django.shortcuts import render, redirect
from .models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from .forms import ReceiptForm, ExpenseCategoryForm, AccountForm
from django.db.models import Count


@login_required
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {"receipts": receipts}
    return render(request, "receipts/receipt_list.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")  
    else:
        form = ReceiptForm()

    context = {"form": form}

    return render(request, "receipts/create_receipt.html", context)

@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    categories_data = categories.annotate(num_receipts=Count('receipts'))
    context = {'expense_categories': categories_data}
    return render(request, 'receipts/expense_category_list.html', context)

@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    account_data = accounts.annotate(num_receipts=Count('receipts'))
    context={'accounts': account_data}
    return render(request, 'receipts/account_list.html', context)

@login_required
def create_category(request):
    if request.method == 'POST':
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            category = form.save(commit=False)
            category.owner = request.user
            category.save()
            return redirect('category_list')  
    else:
        form = ExpenseCategoryForm()
    
    context = {'form': form}
    return render(request, 'categories/create_category.html', context)

@login_required
def create_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            account.owner = request.user
            account.save()
            return redirect('account_list') 
    else:
        form = AccountForm()
    
    context = {'form': form,}
    return render(request, 'accounts/create_account.html', context)